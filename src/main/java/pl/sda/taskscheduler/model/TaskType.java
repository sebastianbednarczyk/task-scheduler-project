package pl.sda.taskscheduler.model;

/**
 * Created by Koputer on 2016-12-17.
 */
public enum TaskType {
    BUG,
    TASK,
    FEATURE;
}
