This is a taskscheduler project. It was made by Sebastian Bednarczyk and Miłosz Nowak as final project on Software Development Academy Java course. 
Working application can be found at https://taskscheduler.herokuapp.com/

Please note that project is still developed and bacause of using free server can be occasionally unavailable.